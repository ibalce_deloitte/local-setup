# Local setup

Setup your local environment to work with `terraform` and `aws`.
This project is powered by Ansible. 

## Prerequisites
Install [Ansible].
``` 
sudo easy_install pip
sudo pip install ansible --quiet
```

Create an Access Key for each of your AWS accounts/profiles. You will need your `AWS Access Key ID` and `AWS Secret Access Key`
in [`profiles.yml`](ansible/profiles.yml)

Protect your `AWS Secret Access Key` using `ansible-vault`.
``` 
ansible-vault encrypt_string your-aws-secret
```

> You will be asked to provide a new Vault password. The `--ask-vault-pass` option in the next step will prompt you for 
`Vault password: `

## Run the playbook
``` 
ansible-playbook -i ansible/hosts ansible/setup.yml --ask-vault-pass [--ask-become-pass] [--extra-vars bastion_dir=$HOME/projects/infrastructure-bastion-ssh-keys]
```
> Override default values via the `--extra-vars` or `-e` option.

> `--ask-become-pass` is required when running on Linux machines to `sudo` when installing packages, Homebrew doesn't need it.


The end of the expected output should look like:
``` 
PLAY RECAP *************************************************************************************************************
localhost                  : ok=13   changed=1    unreachable=0    failed=0
```

To test whether or not the profiles were correctly configured, run either of the following:
```
aws s3 ls --profile $AWS_PROFILE
aws ec2 describe-instances --profile $AWS_PROFILE
```
> See related article: [Using Profiles with the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)

Incorrectly configured profiles result in the following error:
```
$ aws s3 ls --profile sc-stg-god

An error occurred (SignatureDoesNotMatch) when calling the ListBuckets operation: The request signature we calculated does not match the signature you provided. Check your key and signing method.
```

## FAQ
1. Installed `terraform` version doesn't meet project's version requirements
    ``` 
      Module: root
      Required version: = 0.11.8
      Current version: 0.11.13
    ```
    Uninstall the current version with `brew uninstall terraform`, and install using the raw link of the Formula file 
    specific to the required version.
    
    > See related article: [`Homebrew Install Specific Version of Formula`][brew-install]
    
    Find the specific commit of the desired Formula version via: `git log master -- Formula/terraform.rb`

    To upgrade to the latest version, run `brew upgrade terraform` and use `brew switch terraform` to switch between 
    installed versions.

1. Homebrew installation fails with ["Xcode alone.."][xcode-tools]:

    ``` 
    TASK [Install with homebrew] *******************************************************************************************
    failed: [localhost] (item=[u'terraform', u'python3']) => {"changed": false, "item": ["terraform", "python3"], "msg": "Updating Homebrew...\nError: Xcode alone is not sufficient on High Sierra.\nInstall the Command Line Tools:\n  xcode-select --install"}
    ```
    
    > Updating Homebrew...
    > Error: Xcode alone is not sufficient on High Sierra.
    > Install the Command Line Tools:
    >  xcode-select --install
    
    Run the following command before retrying/running the playbook again.
    
    ```
    xcode-select --install
    ```

1.  Task `Configure AWS profile` fails with "The conditional check .. failed." or "The pexpect python module is required".
    
    Make sure that `ansible_python_interpreter=` of `localhost` in the [hosts file](ansible/hosts) points to the correct 
    python installation, where `pexpect` is installed.
    
    > See related articles: [`Can't find pexpect #31390`][pexpect]

[Ansible]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#intro-installation-guide
[xcode-tools]: https://stackoverflow.com/questions/47569264/brew-install-error-xcode-alone-is-not-sufficient-on-sierra
[pexpect]: https://github.com/ansible/ansible/issues/31390
[brew-install]: https://stackoverflow.com/questions/52921122/homebrew-install-specific-version-of-formula

***
# ToDos
- [ ] Notify correctly when `make ssh-goldeneye` applies changes, currently: `changed_when` condition is true when the `.ssh/goldeneye/config` is first created
- [x] Automate
- [x] Collect all necessary steps to setup the local development environment